$(document).ready(function () 
{
    listar();

    $("#btnregistrar").click(function (e) { 
        e.preventDefault();
        registrar();
    });
});

function registrar()
{
    var nombre  = $("#txtnombre").val();
    var correo  = $("#txtcorreo").val();
    var fecha   = $("#txtfecha").val();
    var pais    = $("#cbopais").val();

    // Mostrar en consola
    console.log("Nombre: ", nombre);
    console.log("Correo: ", correo);
    console.log("Fecha: ", fecha);
    console.log("País: ", pais);

    guardarItem(nombre, correo, fecha, pais);
    listar();
}

function guardarItem(nombre, correo, fecha, pais)
{
    // Verificar si nuestra variable datos no existe
    if (localStorage.getItem("datos") == null)
    {
        var arrayFila       = [1, nombre, correo, fecha, pais];
        var arrayTabla      = [arrayFila];
        var arrayTablaJSON  = JSON.stringify(arrayTabla);
        localStorage.setItem("datos", arrayTablaJSON);
    }
    else
    {
        var arrayTabla = JSON.parse(localStorage.getItem("datos"));
        var posicion = 0;
        for(var i=0; i<arrayTabla.length; i++)
        {
            posicion = i + 1;
            arrayTabla[i][0] = posicion;
        }

        // Insertar a arrayTabla
        var arrayFilaInsertar = [posicion+1, nombre, correo, fecha, pais];
        arrayTabla.push(arrayFilaInsertar);

        // Convertir arrayTabla a JSOn(cadena) y guardar en LocalStorage
        localStorage.setItem("datos", JSON.stringify(arrayTabla));
    }
}

function listar()
{
    if(localStorage.getItem("datos") != null)
    {
        var arrayTabla = JSON.parse(localStorage.getItem("datos"));
        $("#tbldatos").html("");

        for(var i = 0; i<arrayTabla.length; i++)
        {
            var fila = "";
            fila+="<tr>";
            fila+="<td>"+arrayTabla[i][1]+"</td>";
            fila+="<td>"+arrayTabla[i][2]+"</td>";
            fila+="<td>"+arrayTabla[i][3]+"</td>";
            fila+="<td>"+arrayTabla[i][4]+"</td>";
            fila+="<td>"+'<button class="btn btn-sm btn-danger" onclick="eliminar('+arrayTabla[i][0]+')">x</button>'+"</td>";
            fila+="</tr>";
            $("#tbldatos").append(fila);
        }
    }
}

function eliminar(id)
{
    if(localStorage.getItem("datos") != null)
    {
        var arrayTabla = JSON.parse(localStorage.getItem("datos"));

        for(var i = 0; i<arrayTabla.length; i++)
        {
            if(arrayTabla[i][0] == id)
            {
                arrayTabla.splice(i, 1);
            }
        }
        localStorage.setItem("datos", JSON.stringify(arrayTabla));
        listar();
    }
}